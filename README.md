# Spoopify Readme:

This is an application that I created with a group of 3 other people for our python final project.
The main requirement was to use one of the python libraries our professor showed us during class.
Our group decided to use Kivy, which is a mobile application maker, and we happened to be the only group that tried it.

The app is supposed to create a local server that clients can connect to. This will allow them to see a list of songs that they can push to a queue.
The main point of this is so if you're potentially at a party or something, the person playing music does not have to pass their phone around to people who want to request songs, they can just do it from their own devices.

Unfortunately, our web server is down, it was ran through the free student trial of AWS, but it has run out and I don't have a desire to keep it running with this current form of the app.

I do have interest in pursuing this project more, but with a different set of tools (not kivy).
Our group wasn't particularly fond of kivy, while I think it is a cool library, and it definitely has its uses, I'd much rather try something like Xamarin forms since my group all used Android phones and Windows machines.
We never got the app running on our phones because creating an android package was becoming an issue with the resources we had and the time constraint.
I attempted to use Buildozer, but due to me not having a MAC or Linux machine, it was becoming an unnecessary chore for this particular project.

Functionality wise, you can add music to a local queue by entering a number from 0-4 (in our updated code, we have the songs indexed, but it was never committed... which is really the only thing missing here).
Then on the play song screen, you can play the song and either skip to the next song or go back to a previous song.
I'm sure there are plenty of bugs, which is to be expected as this particular code isn't be worked on anymore and due to the project time constraint.

This application runs using Kivy 1.10.1, ffpyplayer (NOT Gstreamer), and Anaconda3.
Not compatible with python2.
As for the song list, I am not uploading any mp3 files of songs (for obvious reasons), you'll have to place your own mp3s into the directory.

